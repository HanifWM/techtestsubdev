package com.xa.assigment.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.sun.istack.NotNull;

@Entity
@Table (name = "User")
public class UserModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private long id;
	
	@Column(name="m_role_id")
	private long mRoleId;
	
	@ManyToOne
	@JoinColumn(name ="m_role_id", insertable = false, updatable = false)
	private RoleModel role;
	
	@Column(name="m_employee_id")
	private long mEmployeeId;
	
	@ManyToOne
	@JoinColumn(name ="m_employee_id", insertable = false, updatable = false)
	private EmployeeModel employee;
	
	@NotNull
	@Column(name="username")
	private String username;
	
	@NotNull
	@Column(name="password")
	private String password;
	
	@NotNull
	@Column(name="is_delete")
	private Boolean isDelete;
	
	@NotNull
	@Column(name="created_by")
	private String createdBy;
	
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="created_date")
	private LocalDate createdDate;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="updated_date")
	private LocalDate updatedDate;
}
