package com.xa.assigment.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.sun.istack.NotNull;

@Entity
@Table (name = "Role")
public class RoleModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private long id;
	
	@NotNull
	@Column(name="code")
	private String code;
	
	@NotNull
	@Column(name="name")
	private String name;
	
	@NotNull
	@Column(name="description")
	private String description;
	
	@NotNull
	@Column(name="is_delete")
	private Boolean is_delete;
	
	@NotNull
	@Column(name="created_by")
	private String createdBy;
	
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="created_date")
	private LocalDate createdDate;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="updated_date")
	private LocalDate updatedDate;
}
