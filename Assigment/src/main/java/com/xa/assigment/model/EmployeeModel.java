package com.xa.assigment.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.sun.istack.NotNull;

@Entity
@Table (name = "Employee")
public class EmployeeModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private long id;
	
	@NotNull
	@Column(name="employee_number")
	private String code;
	
	@NotNull
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="email")
	private String email;
	
	@NotNull
	@Column(name="is_delete")
	private Boolean is_delete;
	
	@NotNull
	@Column(name="created_by")
	private String createdBy;
	
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="created_date")
	private LocalDate createdDate;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="updated_date")
	private LocalDate updatedDate;
}
