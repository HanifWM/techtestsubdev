package com.xa.assigment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xa.assigment.model.CompanyModel;

public interface CompanyRepository extends JpaRepository<CompanyModel, Long> {

}
