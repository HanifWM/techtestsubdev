package com.xa.assigment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xa.assigment.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {

}
