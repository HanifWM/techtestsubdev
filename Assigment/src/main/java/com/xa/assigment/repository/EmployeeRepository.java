package com.xa.assigment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xa.assigment.model.EmployeeModel;

public interface EmployeeRepository extends JpaRepository<EmployeeModel, Long> {

}
