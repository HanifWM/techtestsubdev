package com.xa.assigment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xa.assigment.model.RoleModel;

public interface RoleRepository extends JpaRepository<RoleModel, Long> {

}
