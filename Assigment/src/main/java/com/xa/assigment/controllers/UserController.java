package com.xa.assigment.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xa.assigment.model.EmployeeModel;
import com.xa.assigment.model.RoleModel;
import com.xa.assigment.model.UserModel;
import com.xa.assigment.repository.EmployeeRepository;
import com.xa.assigment.repository.RoleRepository;
import com.xa.assigment.repository.UserRepository;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserRepository userrepo;
	@Autowired
	private RoleRepository rolerepo;
	@Autowired
	private EmployeeRepository employeerepo;

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/user/index");
		List<UserModel> user = this.userrepo.findAll();
		view.addObject("user",user);
		return view;
	}
	
	@GetMapping("add")
	public ModelAndView add() {
		ModelAndView view = new ModelAndView("/user/add");
		List<RoleModel> rolelist = this.rolerepo.findAll();
		List<EmployeeModel> employeelist = this.employeerepo.findAll();
		view.addObject("rolelist",rolelist);
		view.addObject("employeelist",employeelist);
		UserModel user = new UserModel();
		view.addObject("user", user);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute UserModel user, BindingResult result) {
		if(!result.hasErrors()) {
			this.userrepo.save(user);
			return new ModelAndView("redirect:/user/index");
		}
		else {
			return new ModelAndView("redirect:/user/add");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/education/add");
		List<RoleModel> rolelist = this.rolerepo.findAll();
		List<EmployeeModel> employeelist = this.employeerepo.findAll();
		view.addObject("rolelist",rolelist);
		view.addObject("employeelist",employeelist);
		UserModel user = this.userrepo.findById(id).orElse(null);
		view.addObject("user",user);
		return view;
	}
	
	@PostMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id, @ModelAttribute UserModel user) {
		if(this.userrepo.findById(id) != null) {
			this.userrepo.delete(user);
		}
		return new ModelAndView("redirect:/user/index");
	}
}
