package com.xa.assigment.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xa.assigment.model.CompanyModel;
import com.xa.assigment.repository.CompanyRepository;

@Controller
@RequestMapping("/company")
public class CompanyController {
	
	@Autowired
	private CompanyRepository comprepo;

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/company/index");
		List<CompanyModel> comp = this.comprepo.findAll();
		view.addObject("comp",comp);
		return view;
	}
	
	@GetMapping("add")
	public ModelAndView add() {
		ModelAndView view = new ModelAndView("/company/add");
		CompanyModel comp = new CompanyModel();
		view.addObject("comp", comp);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute CompanyModel comp, BindingResult result) {
		if(!result.hasErrors()) {
			this.comprepo.save(comp);
			return new ModelAndView("redirect:/company/index");
		}
		else {
			return new ModelAndView("redirect:/company/add");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/education/add");
		CompanyModel comp = this.comprepo.findById(id).orElse(null);
		view.addObject("comp",comp);
		return view;
	}
	
	@PostMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id, @ModelAttribute CompanyModel comp) {
		if(this.comprepo.findById(id) != null) {
			this.comprepo.delete(comp);
		}
		return new ModelAndView("redirect:/company/index");
	}
}
